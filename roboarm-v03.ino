// Robot arm ananlog joystick controll
// Teppo Rekola 2023
// Based on https://howtomechatronics.com/tutorials/arduino/diy-arduino-robot-arm-with-smartphone-control/

#include <Servo.h>

//joystic mid position
const int deadRangeLow  = 512-50; 
const int deadRangeHigh = 512+50;

const int servoBasePin = 5; // pin for first servo, followed by others +1
const int analogBasePin = 0; // pin for first joystick axis, followed by others +1

const int recPin = 12; // button for record
const int playPin = 13; // button for play recorderd (or pause)
//const int clearPin = 14; // button for clear saved steps ### TODO need pin for this

const int buttonLPin = 4; // pin for button "close"
const int buttonRPin = 11; // pin for button "open"

const int joysticLButtonPin = 2; // pin of left joystick
const int joysticRButtonPin = 3; 


// servos: base rotation, first boon, arm, wrist rotate, wrist tilt, grabber

int initialPositions[] = {90,90,160,80,85,110}; // starting position for all servos
int minPositions[] = {0,0,70,0,0,0}; // limits for all servos, 0-180
int maxPositions[] = {180,160,180,180,180,160};
int speedFactors[] = {80,100,100,100,100,110}; // relative speed for servo 1 - 199, 

int index = 0; // for recording steps
int recDelay = 50; // default time between steps in ms

int steps [50][7]; // recorded steps, seventh value for delay


struct Joint {
  Servo servo;
  int servoPos;
  int servoDiff;
};

Joint r[6]; //base datastructure for robot

void setup() {

  Serial.begin(9600);

  pinMode(recPin, INPUT);
  pinMode(playPin, INPUT);
  //pinMode(clearPin, INPUT);
  pinMode(buttonLPin, INPUT_PULLUP);
  pinMode(buttonRPin, INPUT_PULLUP);
  pinMode(joysticLButtonPin, INPUT_PULLUP);
  pinMode(joysticRButtonPin, INPUT_PULLUP);

  //servo init
  for ( int i = 0; i < 6; ++i )  {
    r[i].servo.attach(servoBasePin + i);
    r[i].servoPos = initialPositions[i];
    r[i].servo.write(r[i].servoPos);
  }


}

void loop() {
  // first three axels basic
  for ( int i = 0; i < 3; ++i )  {
    
    int val = analogRead(analogBasePin +i ); // reads the value of the potentiometer (value between 0 and 1023)

    if ((val > deadRangeLow) && (val < deadRangeHigh)) r[i].servoDiff = 0; // deadzone around center value
    else r[i].servoDiff = (map(val,0,1023,-3,3));
  
    r[i].servoPos = constrain(r[i].servoPos + r[i].servoDiff, minPositions[i], maxPositions[i]); //limit value to valid range    
    r[i].servo.write(r[i].servoPos);                 // sets the servo position according to the scaled value

    // something for speed here
 
    Serial.print(r[i].servoPos);
    Serial.print(", ");
  }  

  //next two selectable by joystic button
  int i;

  Serial.print(digitalRead(joysticRButtonPin));
  Serial.print(": ");
  if (!digitalRead(joysticRButtonPin)){

    // tilt wrist
    i = 3;
  }
  else {
    // rotate wrist 
    i = 4;
  }  


  int val = analogRead(analogBasePin +3 ); // reads the value of the potentiometer 3 (value between 0 and 1023)

  if ((val > deadRangeLow) && (val < deadRangeHigh)) r[i].servoDiff = 0; // deadzone around center value
  else r[i].servoDiff = (map(val,0,1023,-3,3));

  r[i].servoPos = constrain(r[i].servoPos + r[i].servoDiff, minPositions[i], maxPositions[i]); //limit value to valid range    
  r[i].servo.write(r[i].servoPos);                 // sets the servo position according to the scaled value

  // something for speed here

  Serial.print(r[3].servoPos);
  Serial.print(", ");
  Serial.print(r[4].servoPos); 
  Serial.print(", ");

  //last by digital button
  i = 5;
  r[i].servoDiff = 0;



  if (!digitalRead(buttonLPin)){
    // close gripper
    r[i].servoDiff = -3;

  }

  if (!digitalRead(buttonRPin)){
    // open gripper
    r[i].servoDiff = 3;
  }

  r[i].servoPos = constrain(r[i].servoPos + r[i].servoDiff, minPositions[i], maxPositions[i]); //limit value to valid range    
  r[i].servo.write(r[i].servoPos);                 // sets the servo position according to the scaled value

  // something for speed here

  Serial.print(r[i].servoPos);
  Serial.print(", ");

  
  Serial.println();
  delay(15);                           // waits for the servo to get there

  // save step
  if (digitalRead(recPin)){
    if (index < 50) {
     
      for ( int i = 0; i < 6; ++i )  {
        steps[index][i] = r[i].servoPos;
      }
      steps[index][6] = recDelay;
      index++;
      //debounce and avoid double steps
      delay(1000);
    }
    // fail somehow here, memory full
  }

  //clear data
  //if (digitalRead(clearPin)){
  //  memset(steps, 0, sizeof(steps));
  //  index = 0;
  //  //debounce and avoid double steps
  //  delay(1000);    
  //} 

  //play data
  if (digitalRead(playPin)){
    for ( int step = 0; step < sizeof(steps); ++step ) {
        Serial.print("PLAY: ");
        for ( int i = 0; i < 6; ++i )  {
    
          r[i].servo.write(steps[step][i]); 
          Serial.print(steps[step][i]);
          Serial.print(", ");
          delay (steps[step][6]);
        }  

        // clear and exit if button
        //if (digitalRead(clearPin)){
        //  memset(steps, 0, sizeof(steps));
        //  index = 0;
        //  break;
        //}

         // stop if button pressed
        if (digitalRead(playPin)){
          //check button is still pressed
          delay(500);
          if (digitalRead(playPin)){          
            break;
          }
        }
    }
  }    
}
