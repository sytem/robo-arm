# robo-arm

Arduino code for small robot arm, based on https://howtomechatronics.com/tutorials/arduino/diy-arduino-robot-arm-with-smartphone-control/

Controll arm with 2 joysticks and button, like an Excavator ( https://en.wikipedia.org/wiki/Excavator_controls#/media/File:Minibagger_Beschreibung.jpg )


Used Arduino Nano, other will work

Project log (in Finnish) and pictures can be found https://konekansa.net/threads/kotikoon-robottik%C3%A4sivarsi.64154/

